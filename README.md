# company-reconciliation

Datasets are available in : s3://d-ew1-ted-ai-experiments-data/company-reconciliation/

## Prerequisites
- python 3.9

## Install python requirements in a virtual environment
```shell
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```


